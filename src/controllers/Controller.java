package controllers;

import entry_point.Main;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import utils.Constants;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;


public class Controller {

    @FXML
    private TilePane tile;
    @FXML
    private VBox vBoxTree;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private TreeView<File> treeView;

    private ImageView imageView;
    private Main main;
    private FXMLLoader fxmlLoader = new FXMLLoader();
    private Parent parent;
    private EditImageWindowController editImageWindowController;
    private Stage editImageWindowStage;
    private Stage mainStage;
    private File[] imagesArr;
    private File folderWithImages;

    @FXML
    private void initialize() {
        initTreeView();
        initLoader();
    }

    private void initTreeView() {
        RootTreeItem root = new RootTreeItem(new File(Constants.ROOT_DIRECTORY_D));
        treeView.setRoot(root);
        treeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<File>>() {
            @Override
            public void changed(ObservableValue<? extends TreeItem<File>> observable, TreeItem<File> oldValue, TreeItem<File> newValue) {
                final TreeItem<File> selectedItem = newValue;
                tile.getChildren().clear();
                String path = selectedItem.getValue().getPath();
                if (!path.isEmpty() && !path.equals("")) {
                    folderWithImages = new File(path);
                    imagesArr = folderWithImages.listFiles();
                    if (imagesArr != null) {
                        for (File file : imagesArr) {
                            String typeFile = new MimetypesFileTypeMap().getContentType(file).split("/")[0];
                            if (file.isFile() && file.exists() && typeFile.equals("image")) {
                                ImageView imageView = createImageView(file);
                                tile.getChildren().addAll(imageView);
                            }
                        }
                    }
                }
            }
        });
    }

    private void initLoader() {
        try {
            fxmlLoader.setLocation(getClass().getResource("../fxml/layout_edit_image.fxml"));
            parent = fxmlLoader.load();
            editImageWindowController = fxmlLoader.getController();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEditImageWindow(Image imageFile, File file) {
        if (editImageWindowStage == null) {
            editImageWindowStage = new Stage();
            editImageWindowStage.setTitle("Edit picture");
            editImageWindowStage.getIcons().add(new Image(getClass().getResource("/drawable/ic_gallery.png").toString()));
            editImageWindowStage.setResizable(false);
            editImageWindowStage.setScene(new Scene(parent));
            editImageWindowStage.initModality(Modality.WINDOW_MODAL);
            editImageWindowStage.initOwner(mainStage);
            editImageWindowController.setMain(main);
            editImageWindowStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    updateTilePane();
                }
            });
        }
        editImageWindowController.setOriginalImage(imageFile);
        editImageWindowController.setOriginalImageFile(file);
        editImageWindowStage.showAndWait();


    }

    private void updateTilePane() {
        tile.getChildren().clear();
        imagesArr = folderWithImages.listFiles();
        if (imagesArr != null) {
            for (File file : imagesArr) {
                String typeFile = new MimetypesFileTypeMap().getContentType(file).split("/")[0];
                if (file.isFile() && file.exists() && typeFile.equals("image")) {
                    ImageView imageView = createImageView(file);
                    tile.getChildren().addAll(imageView);
                }
            }
        }
    }

    private ImageView createImageView(final File imageFile) {
        try {
            final Image image = new Image(imageFile.toURI().toURL().toString(), true);
            imageView = new ImageView(image);
            imageView.fitHeightProperty().setValue(170);
            imageView.fitWidthProperty().setValue(170);
            imageView.setPreserveRatio(true);
            imageView.setCache(true);
            imageView.setSmooth(true);
            tile.setPadding(new Insets(10, 10, 10, 10));
            imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                        if (mouseEvent.getClickCount() == 2) {
                            showEditImageWindow(image, imageFile);
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageView;
    }

    public void setMain(Main pMain) {
        main = pMain;
    }

    public void setMainStage(Stage pStage) {
        mainStage = pStage;
    }
}
