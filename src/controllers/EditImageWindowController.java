package controllers;

import entry_point.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.io.IOException;

public class EditImageWindowController {

    @FXML
    private ImageView ivEditImage;
    @FXML
    private Slider sliderBlurFilter;
    @FXML
    private Slider slTritoneHeight;
    @FXML
    private Slider slTritoneShadow;
    @FXML
    private Slider slTritoneMid;
    @FXML
    private Slider slContrast;
    @FXML
    private Slider slBrightness;
    @FXML
    private Slider slAngle;
    @FXML
    private Slider slRadius;
    @FXML
    private Slider slSides;
    @FXML
    private Slider slRange;
    @FXML
    private Slider slLevel;
    @FXML
    private Slider slXAmpl;
    @FXML
    private Slider slYAmpl;
    @FXML
    private Slider slXWave;
    @FXML
    private Slider slYWave;


    private Image originalImage;
    private Main main;
    private FiltersWrapper filtersWrapper;
    private File originalImageFile;
    private File editedImageFile;
    private Image editedImage;

    @FXML
    private void initialize() {
        filtersWrapper = new FiltersWrapper();
    }

    public void setOriginalImage(Image originalImage) {
        this.originalImage = originalImage;
        ivEditImage.setImage(originalImage);
        ivEditImage.fitHeightProperty().setValue(410);
        ivEditImage.fitWidthProperty().setValue(650);
        ivEditImage.setPreserveRatio(true);
        ivEditImage.setCache(true);
        ivEditImage.setSmooth(true);
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public void actionButtonPressed(ActionEvent actionEvent) throws IOException {
        Object sourse = actionEvent.getSource();
        if (!(sourse instanceof Button)) {
            return;
        }
        Button clickedButton = (Button) sourse;
        switch (clickedButton.getId()) {
            case "btBlurFilter":
                editedImageFile = filtersWrapper.blur(originalImageFile,
                        (float) sliderBlurFilter.getValue(), 2);
                break;
            case "btTritoneFilter":
                editedImageFile = filtersWrapper.tritoneFilter(originalImageFile,
                        (int) slTritoneHeight.getValue(),
                        (int) slTritoneShadow.getValue(),
                        (int) slTritoneMid.getValue());
                break;
            case "btContrastFilter":
                editedImageFile = filtersWrapper.contrastFilter(originalImageFile,
                        (float) slBrightness.getValue(),
                        (float) slContrast.getValue());
                break;
            case "btKaleidoscopeFilter":
                editedImageFile = filtersWrapper.kaleidoscopeFilter(originalImageFile,
                        (float) slRadius.getValue(),
                        (float) slAngle.getValue(),
                        (int) slSides.getValue());
                break;
            case "btOilFilter":
                editedImageFile = filtersWrapper.oilFilter(originalImageFile,
                        (int) slRange.getValue(),
                        (int) slLevel.getValue());
                break;
            case "btRippleFilter":
                editedImageFile = filtersWrapper.rippleFilter(originalImageFile,
                        (float) slXAmpl.getValue(),
                        (float) slYAmpl.getValue(),
                        (float) slXWave.getValue(),
                        (float) slYWave.getValue());
                break;

        }
        editedImage = new Image(editedImageFile.toURI().toURL().toString(), true);
        ivEditImage.setImage(editedImage);
    }

    public void setOriginalImageFile(File originalImageFile) {
        this.originalImageFile = originalImageFile;
    }

    public void actionSaveClick(ActionEvent pActionEvent) {
        main.getSaveUtils().save(originalImageFile, editedImageFile);
    }

    public void actionSaveAsClick(ActionEvent pActionEvent) {
        main.getSaveUtils().saveAs(editedImageFile);
    }

    public void actionCloseClick(ActionEvent pActionEvent) {
        main.getSaveUtils().handleExit();
    }

    public void actionAboutClick(ActionEvent pActionEvent) {
        main.getSaveUtils().handleAbout();
    }
}
