package controllers;

import filters.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FiltersWrapper {

    public FiltersWrapper() {
    }

    protected File blur(File srcFile, float radius, int iterations) throws IOException {
        BufferedImage srcImage = ImageIO.read(srcFile);
        BufferedImage destImage = ImageIO.read(srcFile);
        BoxBlurFilter boxBlurFilter = new BoxBlurFilter();
        boxBlurFilter.setRadius(radius);
        boxBlurFilter.setIterations(iterations);
        destImage = boxBlurFilter.filter(srcImage, destImage);
        File file = getFile(destImage);
        return file;
    }

    protected File tritoneFilter(File srcFile, int heigh, int shadow, int mid) throws IOException {
        BufferedImage srcImage = ImageIO.read(srcFile);
        BufferedImage destImage = ImageIO.read(srcFile);

        TritoneFilter tritoneFilter = new TritoneFilter();
        tritoneFilter.setHighColor(heigh);
        tritoneFilter.setShadowColor(shadow);
        tritoneFilter.setMidColor(mid);

        destImage = tritoneFilter.filter(srcImage, destImage);
        File file = getFile(destImage);
        return file;
    }

    protected File contrastFilter(File srcFile, float brightness, float contrast) throws IOException {
        BufferedImage srcImage = ImageIO.read(srcFile);
        BufferedImage destImage = ImageIO.read(srcFile);

        ContrastFilter contrastFilter = new ContrastFilter();
        contrastFilter.setBrightness(brightness);
        contrastFilter.setContrast(contrast);

        destImage = contrastFilter.filter(srcImage, destImage);
        File file = getFile(destImage);
        return file;
    }

    protected File kaleidoscopeFilter(File srcFile, float radius, float angle, int sides) throws IOException {
        BufferedImage srcImage = ImageIO.read(srcFile);
        BufferedImage destImage = ImageIO.read(srcFile);

        KaleidoscopeFilter kaleidoscopeFilter = new KaleidoscopeFilter();
        kaleidoscopeFilter.setRadius(radius);
        kaleidoscopeFilter.setAngle(angle);
        kaleidoscopeFilter.setSides(sides);

        destImage = kaleidoscopeFilter.filter(srcImage, destImage);
        File file = getFile(destImage);
        return file;
    }

    protected File oilFilter(File srcFile, int levels, int range) throws IOException {
        BufferedImage srcImage = ImageIO.read(srcFile);
        BufferedImage destImage = ImageIO.read(srcFile);

        OilFilter oilFilter = new OilFilter();
        oilFilter.setLevels(levels);
        oilFilter.setRange(range);

        destImage = oilFilter.filter(srcImage, destImage);
        File file = getFile(destImage);
        return file;
    }

    protected File rippleFilter(File srcFile, float XAmpl, float YAmpl, float XWave, float YWave) throws IOException {
        BufferedImage srcImage = ImageIO.read(srcFile);
        BufferedImage destImage = ImageIO.read(srcFile);

        RippleFilter rippleFilter = new RippleFilter();
        rippleFilter.setXAmplitude(XAmpl);
        rippleFilter.setYAmplitude(YAmpl);
        rippleFilter.setXWavelength(XWave);
        rippleFilter.setYWavelength(YWave);

        destImage = rippleFilter.filter(srcImage, destImage);
        File file = getFile(destImage);
        return file;
    }


    private File getFile(BufferedImage destImage) throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        ImageIO.write(destImage, "png", outStream);
        File file = new File("temp.png");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(outStream.toByteArray());
        return file;
    }
}
