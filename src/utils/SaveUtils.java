package utils;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class SaveUtils {
    private Stage mPrimaryStage;

    public SaveUtils(Stage pPrimaryStage) {
        this.mPrimaryStage = pPrimaryStage;
    }

    public void save(File originalImageFile, File editedImageFile) {
        if (originalImageFile != null && editedImageFile != null) {
            saveImageToFile(originalImageFile, editedImageFile);
        }
    }

    public void saveAs(File editedImageFile) {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter1 = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.jpg");
        fileChooser.getExtensionFilters().add(extFilter1);
        File file = fileChooser.showSaveDialog(mPrimaryStage);
        if (file != null) {
            saveImageToFile(file, editedImageFile);
        }
    }

    public void saveImageToFile(File destinationImageFile, File editedImageFile) {
        try {
            BufferedImage bufferedEditedImage = ImageIO.read(editedImageFile);
            writeImage(bufferedEditedImage, destinationImageFile.getPath());
        } catch (Exception e) {
            FxDialogs.showInformation("Error", "Could not save image to file:\n" + destinationImageFile.getPath());
        }
    }

    private void writeImage(BufferedImage destImage, String path) throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        ImageIO.write(destImage, "jpg", outStream);
        File file = new File(path);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(outStream.toByteArray());
    }

    public void handleAbout() {
        FxDialogs.showInformation("Blueberries gallery application", "Creator: Yuriy Getsko, 525st");
    }

    public void handleExit() {
        System.exit(0);
    }
}
