package utils;

public class Constants {

    public static final int ROOT_VIEW_WIDTH = 900;
    public static final int ROOT_VIEW_HEIGHT = 500;
    public static final String ROOT_DIRECTORY_D = "D:/";
    public static final String ROOT_DIRECTORY_C = "C:/";

    public static final String RECYCLE_BIN = "$RECYCLE.BIN";

}
