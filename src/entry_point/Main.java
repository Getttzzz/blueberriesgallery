package entry_point;

import controllers.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import utils.Constants;
import utils.SaveUtils;

public class Main extends Application {

    private Stage mPrimaryStage;
    private SaveUtils mSaveUtils;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.mPrimaryStage = primaryStage;

        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("../fxml/main.fxml"));
        Parent parent = fxmlLoader.load();

        Controller controller = fxmlLoader.getController();
        controller.setMainStage(mPrimaryStage);
        controller.setMain(this);

        mPrimaryStage.setTitle("Blueberries Gallery");
        mPrimaryStage.setMinWidth(Constants.ROOT_VIEW_WIDTH);
        mPrimaryStage.setMinHeight(Constants.ROOT_VIEW_HEIGHT);
        mPrimaryStage.setMaxWidth(Constants.ROOT_VIEW_WIDTH);
        mPrimaryStage.setMaxHeight(Constants.ROOT_VIEW_HEIGHT);
        mPrimaryStage.getIcons().add(new Image(getClass().getResource("/drawable/ic_gallery.png").toString()));
        mPrimaryStage.setScene(new Scene(parent, Constants.ROOT_VIEW_WIDTH, Constants.ROOT_VIEW_HEIGHT));
        mPrimaryStage.show();

        mSaveUtils = new SaveUtils(mPrimaryStage);
    }

    public SaveUtils getSaveUtils() {
        return mSaveUtils;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
